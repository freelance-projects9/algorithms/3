import java.util.Scanner;

public class Main {
    static int n;

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        System.out.println("Enter n:");
        n = Integer.parseInt(in.nextLine());
        boolean[] matrix = new boolean[n];

        algorithm(matrix, n);

    }

    static void algorithm(boolean[] matrix, int level) {
        if (level == 0) {
            for (boolean b : matrix) {
                if (b) {
                    System.out.print("K");
                } else {
                    System.out.print("J");
                }
            }
            System.out.println();
            return;
        }

        algorithm(matrix, level - 1);
        matrix[n - level] = true;
        algorithm(matrix, level - 1);
        matrix[n - level] = false;

    }

}
