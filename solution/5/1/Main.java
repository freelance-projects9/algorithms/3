import java.util.Scanner;

public class Main {
    static int number;

    static void FindTarget(int[] arrayOfSolution, int number,String moveAheadOrBacktrack) {
        if (number == 0) {
            int i =0;
            while(i < arrayOfSolution.length){
                if (arrayOfSolution[i] == 1) {
                    System.out.print("K");
                } else {
                    System.out.print("J");
                }
                i++;
            }
            System.out.println();
            return;
        }
        FindTarget(arrayOfSolution, number - 1,"move ahead");
             arrayOfSolution[Main.number - number] = 1;
        FindTarget(arrayOfSolution, number - 1,"Backtrack");
             arrayOfSolution[Main.number - number] = 0;
    }

    public static void main(String[] args) {
        System.out.println("enter the number");
        Scanner sc = new Scanner(System.in);
        boolean isDone = false;
        while (!isDone) {
            try {
                number = Integer.parseInt(sc.nextLine());
                int[] array = new int[number];
                FindTarget(array, number,"moveAheadOrBacktrack");
                isDone = true;
            } catch (Exception e) {
                System.out.println("## Wrong Input ## \n Please enter an Integer :");
            }
        }
    }
}
