import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    static Character[] result;

    public static void main(String[] args) {
        int n;
        Scanner in = new Scanner(System.in);
        System.out.println("enter the number");
        n = Integer.parseInt(in.nextLine());
        result = new Character[n];
        algorithm(0);
    }

    private static void algorithm(int index) {
        if (index == result.length) {
            for (Character ch : result) {
                System.out.print(ch);
            }
            System.out.println();
            return;
        }
        result[index] = 'J';
        algorithm(index + 1);
        result[index] = 'K';
        algorithm(index + 1);

    }

}
