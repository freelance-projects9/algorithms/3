
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class Main4 {

    /*
     *  Declare variables & data structures
     */
    static int numOfProposedSites = 0;
    static int[] costs = new int[0];
    static String[] hospitalsNames = new String[0];
    static HashMap<String, String[]> strictsToBeCovered;
    static long availableFund = 0;
    /// for the algorithm
    static int optimalCoverage = 0;
    static ArrayList<String> optimalCoverageSites;
    static ArrayList<String> tempSolution;


    public static void main(String[] args) {
        try {
            /*
             *  Reading inputs from file
             */
            File inputFile = new File("src\\input.txt");
            if (inputFile.exists()) {

                Scanner input = new Scanner(inputFile);
                availableFund = input.nextLong();
                input.nextLine();
                numOfProposedSites = input.nextInt();
                input.nextLine();

                // Initializing matrices    //////////////////////////
                hospitalsNames = new String[numOfProposedSites];
                strictsToBeCovered = new HashMap<>();
                costs = new int[numOfProposedSites];
                optimalCoverageSites = new ArrayList<>();
                tempSolution = new ArrayList<>();
                //////////////////////////////////////////////////////

                int i = 0;
                String line;
                while (true) {
                    try {
                        line = input.nextLine();
                    } catch (Exception e) {
                        break;
                    }
                    // Parsing a single line from file
                    String[] words = line.split(" ");
                    hospitalsNames[i] = words[0];

                    strictsToBeCovered.put(words[0],new String[words.length - 3]);
                    for (int j = 1; j < words.length - 2; j++) {
                        strictsToBeCovered.get(words[0])[j - 1] = words[j];
                    }
                    costs[i] = Integer.parseInt(words[words.length - 1]);
                    i++;
                }

            } else {
                System.out.println("Input file does not exist!");
                return;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        /*
         *  Run the algorithm
         */
        run(0, availableFund);

        /*
         *  Print the result
         */
        System.out.print("The optimal coverage is:  ");
        System.out.print("[");
        for (String site : optimalCoverageSites) {
            System.out.print(site + " ");
        }
        System.out.print("]\nCovers ( " + optimalCoverage + " ) stricts.");
    }


    static void run(int index, long fund) {
        if (fund <= 0 || index == numOfProposedSites) {
            // reached a solution ==> find out the coverage
            HashSet<String> selectedStricts = new HashSet<>();
            for(String site : tempSolution){
                selectedStricts.addAll(Arrays.asList(strictsToBeCovered.get(site)));
            }
            // Check optimality by comparing the result with the previous optimal solution
            if (selectedStricts.size() >= optimalCoverage) {
                // updating optimal solution

                optimalCoverageSites = new ArrayList<>();
                optimalCoverageSites.addAll(tempSolution);
                optimalCoverage = selectedStricts.size();
            }
            return;
        }
        // Check if we can select current site to be a part of the solution
        if (fund > costs[index]) {
            tempSolution.add(hospitalsNames[index]);
            run(index + 1, fund - costs[index]);
            tempSolution.remove(hospitalsNames[index]);
        }
        // move to next site without selecting current site to be part of the solution
        run(index + 1, fund);
    }
}