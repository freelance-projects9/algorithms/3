import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class Main {

    static long optimalCoverage = 0;
    static int N;
    static String[] names;
    static HashMap<String, ArrayList<String>> stricts = new HashMap<>();
    static long fund = 0;
    static long[] costs;
    static ArrayList<String> bestSolution = new ArrayList<>();


    static void readInput() {
        File inputFile = new File("src\\input.txt");
        if (!inputFile.exists()) {
            System.out.println("Input file does not exist!");
            return;
        }
        Scanner input;
        try {
            input = new Scanner(inputFile);
        } catch (FileNotFoundException e) {
            return;
        }
        fund = input.nextLong();
        input.nextLine();
        N = input.nextInt();
        input.nextLine();
        names = new String[N];
        costs = new long[N];

        String line = input.nextLine();
        for (int i = 0; i < N; i++) {
            parseLine(line, i);
            try {
                line = input.nextLine();
            } catch (Exception e) {
                break;
            }
        }
    }

    private static void parseLine(String line, int i) {
        String[] words = line.split(" ");
        names[i] = words[0];
        ArrayList<String> siteStricts = new ArrayList<>();
        int j = 1;
        while (j < words.length - 2) {
            siteStricts.add(words[j]);
            j++;
        }
        stricts.put(words[0], siteStricts);
        costs[i] = Long.parseLong(words[words.length - 1]);
    }

    public static void main(String[] args) {
        readInput();
        algorithm(0, 0, new ArrayList<>());

        print_result();
    }

    static void print_result() {
        System.out.print("Optimal Solution is:  ");
        System.out.println(bestSolution);
        System.out.print("Optimal Coverage is:  ");
        System.out.println(optimalCoverage);
    }


    static void algorithm(int index, long total, ArrayList<String> solution) {
        if (index != N && total < fund) {
            if (costs[index] + total < fund) {
                solution.add(names[index]);
                algorithm(index + 1, total + costs[index], solution);
                solution.remove(names[index]);
            }
            algorithm(index + 1, total, solution);
        }
        HashSet<String> allStricts = new HashSet<>();
        for (String site : solution) {
            allStricts.addAll(stricts.get(site));
        }
        if (allStricts.size() >= optimalCoverage) {
            bestSolution = new ArrayList<>();
            bestSolution.addAll(solution);
            optimalCoverage = allStricts.size();
        }

    }

}
