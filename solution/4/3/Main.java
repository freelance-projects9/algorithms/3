import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    /// for the algorithm
    static boolean[] selectionMatrix;
    static int perfectCoverage = 0;
    static boolean[] perfectCoverageSites;

    //Declare variables & data structures
    static int numberRequiredSites = 0;
    static int[] amount = new int[0];
    static String[] NamesOfHospitals = new String[0];
    static String[][] FinallStricts;
    static int availableMoney = 0;

    static void solver(int counter, int fund) {
        if (fund == 0 || counter == numberRequiredSites) {
            ArrayList<String> selectedStricts = new ArrayList<>();
            // find the the coverage
            int i = 0;
            while (i < numberRequiredSites) {
                if (selectionMatrix[i]) {
                    for (String strict : FinallStricts[i]) {
                        if (!selectedStricts.contains(strict)) {
                            selectedStricts.add(strict);
                        }
                    }
                }
                i++;
            }
            // Check optimality by comparing the result with the previous perfect solution
            if (selectedStricts.size() >= perfectCoverage) {
                // updating perfect solution
                perfectCoverageSites = selectionMatrix.clone();
                perfectCoverage = selectedStricts.size();
            }
            return;
        }
        // Check if we can select current site to be a part of the solution
        if (fund > amount[counter]) {
            selectionMatrix[counter] = true;
            solver(counter + 1, fund - amount[counter]);
            selectionMatrix[counter] = false;
        }
        // move to next site without selecting current site to be part of the solution
        solver(counter + 1, fund);
    }

    static void readData(Scanner sc){
        availableMoney = sc.nextInt();
        sc.nextLine();
        numberRequiredSites = sc.nextInt();
        sc.nextLine();
    }

    static void init(){
        // Initializing matrices
        NamesOfHospitals = new String[numberRequiredSites];
        FinallStricts = new String[numberRequiredSites][];
        amount = new int[numberRequiredSites];
        selectionMatrix = new boolean[numberRequiredSites];
        perfectCoverageSites = new boolean[numberRequiredSites];
        //......................................................
    }

    public static void main(String[] args) {
        Scanner sc;

        try {
            //Reading inputs from file
            File inputFile = new File("src\\input.txt");
            sc = new Scanner(inputFile);
            readData(sc);
            init();
            int i = 0;
            String line;
            for (int j = 0; i < numberRequiredSites; j++) {
                line = sc.nextLine();

                // Parsing a single line from file
                String[] words = line.split(" ");
                NamesOfHospitals[i] = words[0];
                int size = words.length;
                FinallStricts[i] = new String[size - 3];
                for (int k = 1; k < size - 2; k++) {
                    FinallStricts[i][k - 1] = words[k];
                }
                amount[i] = Integer.parseInt(words[size - 1]);
                i++;
            }
            // Printing parsed data

        } catch (Exception e) {
            e.printStackTrace();
        }

        //Run the algorithm
        solver(0, availableMoney);

        //Print the result
        System.out.print("The perfect coverage is:  ");
        System.out.print("[");
        int counter = 0;
        while (counter < numberRequiredSites) {
            if (perfectCoverageSites[counter]) {
                System.out.print(NamesOfHospitals[counter] + ", ");
            }
            counter++;
        }
        System.out.print("]\nCovers < " + perfectCoverage + " > stricts.");
    }
}