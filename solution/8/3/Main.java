import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class Main {


    static long optimalCoverage = 0;
    static int N;
    static ArrayList<String> names = new ArrayList<>();
    static HashMap<String, ArrayList<String>> stricts = new HashMap<>();
    static long fund = 0;
    static ArrayList<Integer> costs = new ArrayList<>();
    static String[] bestSolution;

    static void readInput() {
        File inputFile = new File("src\\input.txt");
        if (!inputFile.exists()) {
            System.out.println("Input file does not exist!");
            return;
        }
        Scanner input;
        try {
            input = new Scanner(inputFile);
        } catch (FileNotFoundException e) {
            return;
        }
        fund = input.nextLong();
        input.nextLine();
        N = input.nextInt();
        input.nextLine();

        String line = input.nextLine();
        for (int i = 0; i < N; i++) {
            String[] words = line.split(" ");
            names.add(words[0]);
            ArrayList<String> siteStricts = new ArrayList<>();
            for (int j = 1; j < words.length - 2; j++) {
                siteStricts.add(words[j]);
            }
            stricts.put(words[0], new ArrayList<>());
            stricts.get(words[0]).addAll(siteStricts);
            costs.add(Integer.parseInt(words[words.length - 1]));
            try {
                line = input.nextLine();
            } catch (Exception e) {
                break;
            }
        }
    }


    public static void main(String[] args) {

        readInput();

        run(0, fund, new ArrayList<>());

        System.out.print("Optimal Solution is:  ");
        System.out.println(Arrays.toString(bestSolution));
        System.out.print("Optimal Coverage is:  ");
        System.out.println(optimalCoverage);

    }

    static void run(int index, long fund, ArrayList<String> solution) {
        if (index == N || fund == 0) {
            ArrayList<String> allStricts = new ArrayList<>();
            for (String site : solution) {
                for (String x : stricts.get(site)) {
                    if (!allStricts.contains(x)) {
                        allStricts.add(x);
                    }
                }
            }
            if (allStricts.size() >= optimalCoverage) {
                bestSolution = solution.toArray(new String[0]);
                optimalCoverage = allStricts.size();
            }
            return;
        }
        if (costs.get(index) < fund) {
            solution.add(names.get(index));
            run(index + 1, fund - costs.get(index), solution);
            solution.remove(names.get(index));
        }
        run(index + 1, fund, solution);
    }


}
