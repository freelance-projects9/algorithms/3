import java.util.Scanner;

public class Main {
    static int number;

    static void FindTarget(int[] arrayOfSolution, int number) {
        if (number == 0) {
            for (int b : arrayOfSolution) {
                if (b == 1) {
                    System.out.print("K");
                } else {
                    System.out.print("J");
                }
            }
            System.out.println();
            return;
        }
        FindTarget(arrayOfSolution, number - 1);
             arrayOfSolution[Main.number - number] = 1;
        FindTarget(arrayOfSolution, number - 1);
             arrayOfSolution[Main.number - number] = 0;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("enter the number");
        boolean isDone = false;
        while (!isDone) {
            try {
                number = Integer.parseInt(in.nextLine());
                int[] array = new int[number];
                FindTarget(array, number);
                isDone = true;
            } catch (Exception e) {
                System.out.println("## Wrong Input ## \n Please enter an Integer :");
            }
        }
    }
}
