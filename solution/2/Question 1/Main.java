import java.util.Scanner;

public class Main {
    static int n;

    static boolean[] solution;


    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("enter the number");

        n = Integer.parseInt(in.nextLine());
        solution = new boolean[n];
        runAlgorithm(0);

    }

    static void runAlgorithm(int depth) {
        if (depth == n) {
            for (boolean b : solution) {
                if (b) {
                    System.out.print("K");
                } else {
                    System.out.print("J");
                }
            }
            System.out.println();
            return;
        }
        runAlgorithm(depth + 1);
        solution[depth] = true;
        runAlgorithm(depth + 1);
        solution[depth] = false;
    }
}
