import java.util.Scanner;

public class Main {


    static int readInput() {
        System.out.println("Enter N: ");
        Scanner scanner = new Scanner(System.in);
        return Integer.parseInt(scanner.nextLine());
    }

    public static void main(String[] args) {
        int n;
        n = readInput();

        run("", n);

    }

    static void run(String result, int depth){
        if(depth == 0){
            System.out.println(result);
            return;
        }
        run(result + "J", depth-1);
        run(result + "K", depth-1);
    }

}
