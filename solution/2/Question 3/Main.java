import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    // Variables
    static long budget;
    static int n = 0;
    static String[] sites;
    static String[][] coverage;
    static int[] money;
    static boolean[] solution;
    static int optimal = 0;
    static boolean[] optimalSolution;

    static void init() {
        sites = new String[n];
        coverage = new String[n][];
        money = new int[n];
        solution = new boolean[n];
        optimalSolution = new boolean[n];
    }

    public static void main(String[] args) {


        try {
            File file = new File("src\\input.txt");
            Scanner scanner = new Scanner(file);
            budget = scanner.nextLong();
            scanner.nextLine();
            n = scanner.nextInt();
            scanner.nextLine();

            init();
            String nextLine;
            int i = 0;
            do {
                nextLine = scanner.nextLine();
                String[] words = nextLine.split(" ");
                sites[i] = words[0];
                int size = words.length - 3;
                coverage[i] = new String[size];
                for (int j = 1; j <= size; j++) {
                    coverage[i][j - 1] = words[j];
                }
                money[i] = Integer.parseInt(words[words.length - 1]);
                i++;
            } while (i < n);
        } catch (IOException e) {
            System.err.println("Failed to read input file! ");
            e.printStackTrace();
        }

        findOptimalSolution(0, 0);

        System.out.println("Optimal Coverage is: " + optimal);
        System.out.print("Best Selection:   ");
        for (int i = 0; i < n; i++) {
            if (optimalSolution[i]) {
                System.out.print(sites[i] + " ");
            }
        }


    }

    static void findOptimalSolution(int level, long total) {
        if (total == budget || level == n) {
            ArrayList<String> stricts = new ArrayList<>();
            for (int i = 0; i < n; i++) {
                if (solution[i]) {
                    for (String site : coverage[i]) {
                        if (!stricts.contains(site)) {
                            stricts.add(site);
                        }
                    }

                }
            }
            if (stricts.size() > optimal) {
                optimalSolution = solution.clone();
                optimal = stricts.size();
            }
            return;
        }
        findOptimalSolution(level + 1, total);
        if (budget - total > money[level]) {
            solution[level] = true;
            findOptimalSolution(level + 1, total + money[level]);
            solution[level] = false;
        }
    }

}
