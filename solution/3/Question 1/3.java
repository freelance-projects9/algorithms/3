import java.util.Scanner;

public class Main5 {
    static int n;

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("enter the number");
        n = Integer.parseInt(in.nextLine());
        runAlgorithm( 0,"");

    }

    static void runAlgorithm(int depth, String solution) {
        if (depth == n) {
            System.out.println(solution);
            return;
        }
        runAlgorithm(depth + 1,solution + "J");
        runAlgorithm( depth + 1, solution + "K");
    }
}
