import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;


class Site {
    String name;
    long cost;
    String[] coverage;

    Site(String name, long cost, String[] coverage) {
        this.name = name;
        this.cost = cost;
        this.coverage = coverage;
    }
}

public class Main {

    /*
     *  Declare variables & data structures
     */
    static int numOfProposedSites = 0;
    static Site[] sites;
    static long availableFund;
    /// for the algorithm
    static ArrayList<Integer> optimalSolution;
    static ArrayList<Integer> solution;

    static int optimalCoverage = 0;

    public static void main(String[] args) {
        try {
            /*
             *  Reading inputs from file
             */
            File inputFile = new File("src\\input.txt");

            Scanner input = new Scanner(inputFile);
            availableFund = input.nextLong();
            input.nextLine();
            numOfProposedSites = input.nextInt();
            input.nextLine();

            // Initializing matrices    //////////////////////////
            init();
            //////////////////////////////////////////////////////

            String line;
            for (int i = 0; i < numOfProposedSites; i++) {
                line = input.nextLine();
                // Parsing a single line from file
                String[] words = line.split(" ");
                int size = words.length;
                String[] strictsToBeCovered = new String[size - 3];
                for (int j = 1; j < size - 2; j++) {
                    strictsToBeCovered[j - 1] = words[j];
                }
                long cost = Integer.parseInt(words[size - 1]);
                sites[i] = new Site(words[0], cost, strictsToBeCovered);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        /*
         *  Run the algorithm
         */
        run(0, availableFund);

        /*
         *  Print the result
         */
        System.out.print("The optimal coverage is:  ");
        System.out.print("[");
        for (int i : optimalSolution) {
            System.out.print(sites[i].name + ", ");
        }
        System.out.print("]\nCovers ( " + optimalCoverage + " ) stricts.");
    }

    private static void init() {
        sites = new Site[numOfProposedSites];
        optimalSolution = new ArrayList<>();
        solution = new ArrayList<>();
    }


    static void run(int index, long fund) {
        if (fund == 0 || index == numOfProposedSites) {
            ArrayList<String> selectedStricts = new ArrayList<>();
            // reached a solution ==> find out the coverage
            for (int i = 0; i < numOfProposedSites; i++) {
                if (solution.contains(i)) {
                    for (String strict : sites[i].coverage) {
                        if (!selectedStricts.contains(strict)) {
                            selectedStricts.add(strict);
                        }
                    }
                }
            }

            // Check optimality by comparing the result with the previous optimal solution
            if (selectedStricts.size() >= optimalCoverage) {
                // updating optimal solution
                optimalSolution.clear();
                optimalSolution.addAll(solution);
                optimalCoverage = selectedStricts.size();
            }
            return;
        }
// Check if we can select current site to be a part of the solution
        if (fund > sites[index].cost) {
            solution.add(index);
            run(index + 1, fund - sites[index].cost);
            solution.remove(new Integer(index));
        }
        // move to next site without selecting current site to be part of the solution
        run(index + 1, fund);

    }
}