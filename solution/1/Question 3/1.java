import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    /*
     *  Declare variables & data structures
     */
    static int numOfProposedSites = 0;
    static int[] costs = new int[0];
    static String[] hospitalsNames = new String[0];
    static String[][] strictsToBeCovered;
    static long availableFund = 0;
    /// for the algorithm
    static boolean[] selectionMatrix;
    static int optimalCoverage = 0;
    static boolean[] optimalCoverageSites;

    public static void main(String[] args) {
        try {
            /*
             *  Reading inputs from file
             */
            File inputFile = new File("src\\input.txt");
            if (!inputFile.exists()) {
                System.out.println("Input file does not exist!");
                return;
            }
            Scanner input = new Scanner(inputFile);
            availableFund = input.nextLong();
            input.nextLine();
            numOfProposedSites = input.nextInt();
            input.nextLine();

            // Initializing matrices    //////////////////////////
            hospitalsNames = new String[numOfProposedSites];
            strictsToBeCovered = new String[numOfProposedSites][];
            costs = new int[numOfProposedSites];
            selectionMatrix = new boolean[numOfProposedSites];
            optimalCoverageSites = new boolean[numOfProposedSites];
            //////////////////////////////////////////////////////

            int i = 0;
            String line;
            while (true) {
                try {
                    line = input.nextLine();
                } catch (Exception e) {
                    break;
                }
                // Parsing a single line from file
                String[] words = line.split(" ");
                hospitalsNames[i] = words[0];
                strictsToBeCovered[i] = new String[words.length - 3];
                for (int j = 1; j < words.length - 2; j++) {
                    strictsToBeCovered[i][j - 1] = words[j];
                }
                costs[i] = Integer.parseInt(words[words.length - 1]);
                i++;
            }
            // Printing parsed data
            /*
            for (i = 0; i < numOfProposedSites; i++) {
                System.out.println(hospitalsNames[i] + " " + costs[i]);
                for (String strict : strictsToBeCovered[i]) {
                    System.out.print(strict + ", ");
                }
                System.out.println();
            }
            */
        } catch (IOException e) {
            e.printStackTrace();
        }

        /*
         *  Run the algorithm
         */
        run(0, availableFund);

        /*
         *  Print the result
         */
        System.out.print("The optimal coverage is:  ");
        System.out.print("[");
        for (int i = 0; i < numOfProposedSites; i++) {
            if (optimalCoverageSites[i]) {
                System.out.print(hospitalsNames[i] + ", ");
            }
        }
        System.out.print("]\nCovers ( " + optimalCoverage + " ) districts.");
    }


    static void run(int index, long fund) {
        if (fund <= 0 || index == numOfProposedSites) {
            ArrayList<String> selectedStricts = new ArrayList<>();
            // reached a solution ==> find out the coverage
            for (int i = 0; i < numOfProposedSites; i++) {
                if (selectionMatrix[i]) {
                    for (String site : strictsToBeCovered[i]) {
                        if (!selectedStricts.contains(site)) {
                            selectedStricts.add(site);
                        }
                    }
                }
            }
            // Check optimality by comparing the result with the previous optimal solution
            if (selectedStricts.size() >= optimalCoverage) {
                // updating optimal solution
                optimalCoverageSites = selectionMatrix.clone();
                optimalCoverage = selectedStricts.size();
            }
            return;
        }
        // Check if we can select current site to be a part of the solution
        if (fund > costs[index]) {
            selectionMatrix[index] = true;
            run(index + 1, fund - costs[index]);
            selectionMatrix[index] = false;
        }
        // move to next site without selecting current site to be part of the solution
        run(index + 1, fund);
    }
}