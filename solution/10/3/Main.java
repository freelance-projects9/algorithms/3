import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Scanner;


public class Main {

    /*
     *  Declare variables & data structures
     */
    static int numOfProposedSites = 0;
    static HospitalSite[] sites;
    static long availableFund = 0;
    /// for the algorithm
    static ArrayList<HospitalSite> solution;
    static int optimalCoverage = 0;
    static ArrayList<HospitalSite> bestSolution;

    public static void main(String[] args) {
        readData();

        /*
         *  Run the algorithm
         */
        run(0, availableFund);

        /*
         *  Print the result
         */
        System.out.print("The optimal coverage is:  ");
        System.out.print("[");
        for (HospitalSite site : bestSolution) {
            System.out.print(site.name + ", ");
        }
        System.out.print("]\nCovers ( " + optimalCoverage + " ) districts.");
    }

    private static void readData() {
        try {
            /*
             *  Reading inputs from file
             */
            File inputFile = new File("src\\input.txt");
            if (!inputFile.exists()) {
                System.out.println("Input file does not exist!");
                return;
            }

            Scanner input = new Scanner(inputFile);
            availableFund = input.nextLong();
            input.nextLine();
            numOfProposedSites = input.nextInt();
            input.nextLine();

            // Initializing matrices    //////////////////////////
            sites = new HospitalSite[numOfProposedSites];
            solution = new ArrayList<>();
            bestSolution = new ArrayList<>();
            //////////////////////////////////////////////////////

            int i = 0;
            String line;
            while (true) {
                try {
                    line = input.nextLine();
                } catch (Exception e) {
                    break;
                }
                // Parsing a single line from file
                String[] words = line.split(" ");
                String name = words[0];
                String[] stricts = new String[words.length - 3];
                for (int j = 1; j < words.length - 2; j++) {
                    stricts[j - 1] = words[j];
                }
                long cost = Integer.parseInt(words[words.length - 1]);
                sites[i] = new HospitalSite(name, cost, stricts);
                i++;
            }
            // Printing parsed data
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    static void run(int index, long fund) {
        if (fund == 0 || index == numOfProposedSites) {
            HashSet<String> selectedStricts = getSelectedStricts();
            // Check optimality by comparing the result with the previous optimal solution
            if (selectedStricts.size() >= optimalCoverage) {
                // updating optimal solution
                bestSolution = new ArrayList<>();
                bestSolution.addAll(solution);
                optimalCoverage = selectedStricts.size();
            }
            return;
        }
        // Check if we can select current site to be a part of the solution
        if (fund > sites[index].cost) {
            solution.add(sites[index]);
            run(index + 1, fund - sites[index].cost);
            solution.remove(sites[index]);
        }
        // move to next site without selecting current site to be part of the solution
        run(index + 1, fund);
    }

    private static HashSet<String> getSelectedStricts() {
        HashSet<String> result = new HashSet<>();
        // reached a solution ==> find out the coverage
        for (HospitalSite site : solution) {
                result.addAll(Arrays.asList(site.stricts));
        }
        return result;
    }
}


class HospitalSite {

    String name;
    long cost;
    String[] stricts;

    HospitalSite(String name, long cost, String[] stricts) {
        this.cost = cost;
        this.name = name;
        this.stricts = stricts;
    }


}
