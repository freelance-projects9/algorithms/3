import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    static ArrayList<Character> solution;
    static int n;

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        solution = new ArrayList<>();
        System.out.println("enter the number");
        n = Integer.parseInt(in.nextLine());

        run(n);
    }

    static void print_sol(ArrayList<Character> sol) {
        for (Character ch : sol) {
            System.out.print(ch);
        }
        System.out.println();
    }

    static void run(int level) {
        if (level == 0) {
            print_sol(solution);
            return;
        }
        solution.add('J');
        run(level - 1);
        solution.remove(n - level);
        solution.add('K');
        run(level - 1);
        solution.remove(n - level);

    }

}
