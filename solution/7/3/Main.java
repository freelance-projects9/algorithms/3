import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    /// for the algorithm
    static int[] selectionMatrix;
    static float perfectCoverage = 0;
    static int[] perfectCoverageSites;

    //Declare variables & data structures
    static float numberRequiredSites = 0;
    static float[] amount = new float[0];
    static String[] NamesOfHospitals = new String[0];
    static String[][] FinallStricts;
    static float availableMoney = 0;

    static void findingAlgorithm(int counter, float fund) {
        if (fund == 0.0 || counter == numberRequiredSites) {
            List<String> selectedStricts = new ArrayList<>();
            // find the the coverage
            int i = 0;
            boolean isDone = false;
            do {
                if (selectionMatrix[i] == 1) {
                    for (String strict : FinallStricts[i]) {
                        if (!selectedStricts.contains(strict)) {
                            selectedStricts.add(strict);
                        }
                    }
                }
                i++;
                if(i==numberRequiredSites)
                    isDone = true;
            }
            while (!isDone);

            // Check optimality by comparing the result with the previous perfect solution
            if (selectedStricts.size() - perfectCoverage >= 0) {
                // updating perfect solution
                perfectCoverage = selectedStricts.size();
                perfectCoverageSites = selectionMatrix.clone();
            }
            return;
        }
        // Check if we can select current site to be a part of the solution
        if (fund > amount[counter]) {
            selectionMatrix[counter] = 1;
            findingAlgorithm(counter + 1, fund - amount[counter]);
            selectionMatrix[counter] = 0;
        }
        // move to next site without selecting current site to be part of the solution
        findingAlgorithm(counter + 1, fund);
    }

    static void readFile(Scanner sc) {

        availableMoney = sc.nextFloat();
        sc.nextLine();
        numberRequiredSites = sc.nextFloat();
        sc.nextLine();

    }

    static void init(){

        // Initializing matrices
        NamesOfHospitals = new String[(int)numberRequiredSites];
        FinallStricts = new String[(int)numberRequiredSites][];
        amount = new float[(int)numberRequiredSites];
        selectionMatrix = new int[(int)numberRequiredSites];
        perfectCoverageSites = new int[(int)numberRequiredSites];

    }

    public static void main(String[] args) throws FileNotFoundException {
        File inputFile;
            //Reading inputs from file
            try {
                inputFile = new File("src\\input.txt");
            }catch (Exception f){
                System.out.println(f.getMessage());
                return;
            }
        Scanner sc = new Scanner(inputFile);

            readFile(sc);
            init();
            String line;
            int j = 0;
            while (j < numberRequiredSites) {
                line = sc.nextLine();

                // Parsing a single line from file
                String[] words = line.split(" ");
                NamesOfHospitals[j] = words[0];
                FinallStricts[j] = new String[words.length - 3];
                for (int k = 1; k < words.length - 2; k++) {
                    FinallStricts[j][k - 1] = words[k];

                }
                amount[j] = Integer.parseInt(words[words.length - 1]);
                j++;
            }
        //Run the algorithm
        findingAlgorithm(0, availableMoney);

        //Print the result
        System.out.print("The perfect coverage is:  ");
        System.out.print("{ ");
        int counter = 0;
        boolean isDone = false;
        do {
            if (perfectCoverageSites[counter] == 1) {
                System.out.print(NamesOfHospitals[counter] + ", ");
            }
            counter++;
            if(counter == numberRequiredSites)
                isDone = true;
        }
        while (!isDone);
        System.out.print(" }\nCovers << " + (int) perfectCoverage + " >> stricts.");
    }
}