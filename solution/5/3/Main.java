import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    /// for the algorithm
    static int[] selectionMatrix;
    static double perfectCoverage = 0;
    static int[] perfectCoverageSites;

    //Declare variables & data structures
    static int numberRequiredSites = 0;
    static double[] amount = new double[0];
    static String[] NamesOfHospitals = new String[0];
    static String[][] FinallStricts;
    static int availableMoney = 0;

    static boolean solver(int counter, double fund) {
        if (fund == 0.0 || counter == numberRequiredSites) {
            ArrayList<String> selectedStricts = new ArrayList<>();
            // find the the coverage
            int i = 0;
            do {
                if (selectionMatrix[i] == 1) {
                    for (String strict : FinallStricts[i]) {
                        if (!selectedStricts.contains(strict)) {
                            selectedStricts.add(strict);
                        }
                    }
                }
                i++;
            }
            while (i < numberRequiredSites);

            // Check optimality by comparing the result with the previous perfect solution
            if (selectedStricts.size() - perfectCoverage >= 0) {
                // updating perfect solution
                perfectCoverageSites = selectionMatrix.clone();
                perfectCoverage = selectedStricts.size();
            }
            return true;
        }
        // Check if we can select current site to be a part of the solution
        if (fund > amount[counter]) {
            selectionMatrix[counter] = 1;
            solver(counter + 1, fund - amount[counter]);
            selectionMatrix[counter] = 0;
        }
        // move to next site without selecting current site to be part of the solution
        solver(counter + 1, fund);
        return  true;
    }

    public static void main(String[] args) throws FileNotFoundException {
        File inputFile;
            //Reading inputs from file
            try {
                inputFile = new File("src\\input.txt");
            }catch (Exception f){
                System.out.println(f.getMessage());
                return;
            }

            Scanner sc = new Scanner(inputFile);
            availableMoney = sc.nextInt();
            sc.nextLine();
            numberRequiredSites = sc.nextInt();
            sc.nextLine();

            // Initializing matrices
            NamesOfHospitals = new String[numberRequiredSites];
            FinallStricts = new String[numberRequiredSites][];
            amount = new double[numberRequiredSites];
            selectionMatrix = new int[numberRequiredSites];
            perfectCoverageSites = new int[numberRequiredSites];


            int i = 0;
            String line;
            for (int j = 0; j < numberRequiredSites; j++) {
                line = sc.nextLine();

                // Parsing a single line from file
                String[] words = line.split(" ");
                NamesOfHospitals[i] = words[0];
                FinallStricts[i] = new String[words.length - 3];
                int k = 1;
                do {
                    FinallStricts[i][k - 1] = words[k];
                    k++;
                }
                while (k < words.length - 2);
                amount[i] = Integer.parseInt(words[words.length - 1]);
                i++;
            }
            // Printing parsed data

        //Run the algorithm
        solver(0, availableMoney);

        //Print the result
        System.out.print("The perfect coverage is:  ");
        System.out.print("[");
        int counter = 0;
        do {
            if (perfectCoverageSites[counter] == 1) {
                System.out.print(NamesOfHospitals[counter] + ", ");
            }
            counter++;
        }
        while (counter < numberRequiredSites);

        System.out.print("]\nCovers ( " + perfectCoverage + " ) stricts.");
    }
}