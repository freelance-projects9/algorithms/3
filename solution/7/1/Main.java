import java.util.Scanner;

public class Main {
    static int n;
    //this variable ( moveAheadOrBacktrack ) just to know what recursion work
    static void FindTarget(double[] arrayOfSolution, int number, int moveAheadOrBacktrack) {
        if (number == 0) {
            for (double b : arrayOfSolution) {
                if (b == 1.0) {
                    System.out.print("K");
                } else {
                    System.out.print("J");
                }
            }
            System.out.println();
            return;
        }
        FindTarget(arrayOfSolution, number - 1, 1);
        arrayOfSolution[n - number] = 1.0;
        FindTarget(arrayOfSolution, number - 1, 0);
        arrayOfSolution[n - number] = 0.0;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("enter the number");
        int isDone = 0;
        do {
            try {
                n = Integer.parseInt(sc.nextLine());
                double[] array = new double[n];
                FindTarget(array, n, 10);
                isDone = 1;
            } catch (Exception e) {
                System.out.println("## Wrong Input ## \n Please enter an Integer :");
            }
        }
        while (isDone == 0);
    }
}
