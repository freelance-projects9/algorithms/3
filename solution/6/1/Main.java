import java.util.Scanner;

public class Main {
    static int n;
    static Scanner in;
    static boolean[] matrix;

    public static void main(String[] args) {
        takeInput();
        algorithm(n);
    }

    static void takeInput() {
        in = new Scanner(System.in);
        System.out.println("Enter n: ");
        String input = in.nextLine();
        n = Integer.parseInt(input);
        matrix = new boolean[n];

    }

    static void printSolution(){
        for (boolean b : matrix) {
            if (b) {
                System.out.print("K");
            } else {
                System.out.print("J");
            }
        }
        System.out.println();
    }

    static void algorithm(int level) {
        if (level == 0) {
           printSolution();
            return;
        }

        algorithm(level - 1);
        matrix[n - level] = true;
        algorithm(level - 1);
        matrix[n - level] = false;

    }

}
